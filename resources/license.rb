#
# Cookbook:: onlyoffice
# Resource:: onlyoffice_license
#
# Copyright:: 2021, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'onlyoffice_license'

property :content, String, required: true

action :create do
  file '/var/www/onlyoffice/Data/license.lic' do
    content new_resource.content
    owner 'ds'
    group 'ds'
    mode '640'
    sensitive true
    notifies :reload, 'service[supervisor]'
  end
end

action :delete do
  file '/var/www/onlyoffice/Data/license.lic' do
    action :delete
    notifies :reload, 'service[supervisor]'
  end
end
