#
# Cookbook:: onlyoffice
# Resource:: onlyoffice_program
#
# Copyright:: 2021, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

resource_name 'onlyoffice_program'

property :program_name, String, name_property: true
property :parameters, Hash

action :start do
  template "/etc/onlyoffice/documentserver/supervisor/ds-#{new_resource.program_name}.conf" do
    source 'program.conf.erb'
    mode '0644'
    variables(
      name: new_resource.program_name,
      parameters: new_resource.parameters
    )
    notifies :reload, 'service[supervisor]'
  end
  link "/etc/supervisor/conf.d/ds-#{new_resource.program_name}.conf" do
    to "../../onlyoffice/documentserver/supervisor/ds-#{new_resource.program_name}.conf"
    notifies :reload, 'service[supervisor]'
  end
  start_program
end

action :stop do
  template "/etc/onlyoffice/documentserver/supervisor/ds-#{new_resource.program_name}.conf" do
    source 'program.conf.erb'
    mode '0644'
    variables(
      name: new_resource.program_name,
      parameters: new_resource.parameters
    )
    notifies :reload, 'service[supervisor]'
  end
  link "/etc/supervisor/conf.d/ds-#{new_resource.program_name}.conf" do
    action :delete
    notifies :reload, 'service[supervisor]'
  end
  stop_program
end

action_class do
  def start_program
    execute "start #{new_resource.program_name}" do
      command "supervisorctl start ds:#{new_resource.program_name}"
      not_if "supervisorctl status ds:#{new_resource.program_name} | grep -q RUNNING"
    end
  end

  def stop_program
    execute "stop #{new_resource.program_name}" do
      command "supervisorctl stop ds:#{new_resource.program_name}"
      only_if "supervisorctl status ds:#{new_resource.program_name} | grep -q RUNNING"
    end
  end
end
