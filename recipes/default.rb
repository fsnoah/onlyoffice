#
# Cookbook:: onlyoffice
# Recipe:: default
#
# Copyright:: 2021, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

service 'supervisor' do
  supports(
    restart: true,
    reload: true,
    status: true
  )
  action %i(enable start)
end

find_resource :service, 'nginx' do
  action %i(enable start)
end

onlyoffice_install 'install' do
  dbpassword node['onlyoffice']['db']['password']
  dbport node['onlyoffice']['db']['port']
  dbuser node['onlyoffice']['db']['user']
  dbname node['onlyoffice']['db']['name']
  download_package node['onlyoffice']['download_package']
  version node['onlyoffice']['version']
end

onlyoffice_license 'license' do
  content node['onlyoffice']['license'] || 'no license'
  not_if { node['onlyoffice']['license'].nil? }
end

template '/etc/onlyoffice/documentserver/local.json' do
  source 'local.json.erb'
  variables(
    auto_assembly_interval: node['onlyoffice']['auto_assembly_interval'],
    dbhost: node['onlyoffice']['db']['host'],
    dbname: node['onlyoffice']['db']['name'],
    dbpassword: node['onlyoffice']['db']['password'],
    dbport: node['onlyoffice']['db']['port'],
    dbuser: node['onlyoffice']['db']['user'],
    jwt_enable: node['onlyoffice']['jwt']['enable'],
    jwt_header: node['onlyoffice']['jwt']['header'],
    jwt_in_body: node['onlyoffice']['jwt']['in_body'],
    jwt_secret: node['onlyoffice']['jwt']['secret'],
    max_download_bytes: node['onlyoffice']['max_download_bytes'],
    max_file_size: node['onlyoffice']['max_file_size'],
    secret_inbox: node['onlyoffice']['token']['secret_inbox'],
    secret_outbox: node['onlyoffice']['token']['secret_outbox'],
    secret_session: node['onlyoffice']['token']['secret_session'],
    token_enable_browser: node['onlyoffice']['token']['enable_browser'],
    token_enable_request_inbox: node['onlyoffice']['token']['enable_request_inbox'],
    token_enable_request_outbox: node['onlyoffice']['token']['enable_request_outbox']
  )
  notifies :reload, 'service[supervisor]'
end

node['onlyoffice']['programs'].each do |program, attrs|
  enable = attrs['enable']
  attrs = attrs.dup
  attrs.delete 'enable'

  onlyoffice_program program do
    parameters attrs
    action :stop unless enable
  end
end

template '/etc/onlyoffice/documentserver/nginx/ds.conf' do
  source 'ds.conf.erb'
  variables node['onlyoffice']['nginx']
  notifies :reload, 'service[nginx]'
end
