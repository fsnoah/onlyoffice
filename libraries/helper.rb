#
# Cookbook:: onlyoffice
# Library:: helper
#
# Copyright:: 2021, GSI Helmholtzzentrum für Schwerionenforschung
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

module OnlyOffice
  # Helper functions for the OnlyOffice cookbook
  module Helper
    module_function

    def onlyoffice_download_deb_url(version)
      "https://download.onlyoffice.com/install/documentserver/linux/onlyoffice-documentserver-ee_#{version}_amd64.deb"
    end
  end
end
